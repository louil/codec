CFLAGS+=-std=c11 -Wall -Werror -Wno-deprecated -Wextra -Wstack-usage=1024 -pedantic

.PHONY: clean debug

hangman: decoder.o 

debug: CFLAGS+=-g
debug: decoder


clean:
	-rm decoder *.o
