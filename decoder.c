#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

//Declaring the structs to be used in program
struct meditrik{

	unsigned destinationID: 32;
	unsigned sourceID: 32;
	unsigned totalLength: 16;
	unsigned type: 3;
	unsigned int sequenceID: 9;
	unsigned version: 4;
	unsigned payload;
};

struct status{

	unsigned omorfine: 16;
	unsigned capsaicin: 16;
	unsigned glucose: 16;
	double battery;
};

struct gps{

	float altitude;
	double latitude;
	double longitude;
};

struct command{

	unsigned command;
	unsigned int parameter;
};

void ReadFile(int argc, char *argv[]);
int check_tpye(struct meditrik *m1, struct status *s1, struct gps *g1, struct command *c1, unsigned char *buff, int gettomeditrik);
void get_status(struct meditrik *m1, struct status *s1);
void command(struct meditrik *m1, struct status *s1, struct gps *g1, struct command *c1);
void get_gps(struct meditrik *m1, struct gps *g1);
void message(unsigned char *buff, struct meditrik *m1, int gettomeditrik);

int main(int argc, char *argv[]){

	ReadFile(argc, argv);
	
}

//Got from http://www.linuxquestions.org/questions/programming-9/c-howto-read-binary-file-into-buffer-172985/
void ReadFile(int argc, char *argv[]){
	
	//Initializing structs
	struct meditrik m1;
	m1.destinationID = 32;
	m1.sourceID = 32;
	m1.totalLength = 16;
	m1.type = 3;
	m1.sequenceID = 9;
	m1.version = 4;

	struct status s1;
	s1.omorfine = 16;
	s1.capsaicin = 16;
	s1.glucose = 16;
	s1.battery = 64;

	struct gps g1;
	g1.altitude = 32;
	g1.latitude = 64;
	g1.longitude = 64;

	struct command c1;
	c1.command = 16;
	c1.parameter = 16;

	//Setting variables
	char pointtohome[100];
	int z = 0, count = 0, global = 24, ethernet = 14, ipv4 = 20, udp = 8, gettomeditrik = 0 , packetheader = 16;
	FILE *fp;
	unsigned char *buff;
	unsigned int fileLen;
	unsigned int len, diffLen, diff;

	//Error checking for arguments
	if (argc > 2 || argc == 1){
		printf("No file specified to decode.\n");
		exit(0);
	}
	
	//Add argv[1] to the end of the path in pointtohome
	strcpy(pointtohome, "/usr/local/share/codec/");
	strcat(pointtohome, argv[1]);

	//Open file
	fp = fopen(pointtohome, "r");

	//Check to see if the file opened, if it did not then strcpy it allowing it to be opened from the current working directory,
	//then check to see if that will open and exit the program if it can not. 
	if (!fp)
	{
		strcpy(pointtohome, argv[1]);
		fp = fopen(pointtohome, "r");
		if (!fp){
			printf("Unable to open file.\n");
			exit(0);
		}
	}

	//Get file length
	fseek(fp, 0, SEEK_END);
	fileLen = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	//Allocate memory
	buff=(unsigned char *)malloc(fileLen+1);
	if (!buff){
		printf("Memory error!");
    	fclose(fp);
		exit(0);
	}

	//Increment count while looping and setting buff[z] to zero at the same time
	for (z = 0; z < (int)fileLen; z++){
		buff[z] = 0;
		count++;
	}

	//Reads for however many times count is
	fread(buff, 1, count, fp);

	//Set gettomeditrik to the first 82 to start, then use fseek and variables to find out the initial length of the file
	//thus setting the diff variable for the first time
	gettomeditrik = global+packetheader+ethernet+ipv4+udp;
	len = buff[32];
	fseek(fp, 32, SEEK_SET);
	fseek(fp, 0, SEEK_END);
	diffLen = ftell(fp);
	diff = diffLen - len;

	//The main loop of the program that will go through and set every necessary field based upon the information provided
	while(1){
		m1.payload = ((buff[gettomeditrik+2]<<8) + buff[gettomeditrik+3]) - 12;
		m1.type = (buff[gettomeditrik+1] & 7);
		m1.version = (buff[gettomeditrik] >> 4);	
		m1.sequenceID = ((buff[gettomeditrik] & 0xf) << 5) + (buff[gettomeditrik+1] >> 3);
		m1.sourceID = (buff[gettomeditrik+4] << 24) + (buff[gettomeditrik+5] << 16) + (buff[gettomeditrik+6]<<8) + buff[gettomeditrik+7];
		m1.destinationID = (buff[gettomeditrik+8] << 24) + (buff[gettomeditrik+9] << 16) + (buff[gettomeditrik+10]<<8) + buff[gettomeditrik+11];
		struct meditrik *m2;
		m2 = &m1;
		s1.omorfine = (buff[gettomeditrik+24]<<8) + buff[gettomeditrik+25];
		s1.capsaicin = (buff[gettomeditrik+22]<<8) + buff[gettomeditrik+23];
		s1.glucose = (buff[gettomeditrik+20]<<8) + buff[gettomeditrik+21];
		fseek(fp, gettomeditrik+12, SEEK_SET);
		fread(&s1.battery, 8, 1, fp);
		struct status *s2;
		s2 = &s1;
		fseek(fp, gettomeditrik+28, SEEK_SET);
		fread(&g1.altitude, 4, 1, fp);
		fseek(fp, gettomeditrik+20, SEEK_SET);
		fread(&g1.longitude, 8, 1, fp);
		fseek(fp, gettomeditrik+12, SEEK_SET);
		fread(&g1.latitude, 8, 1, fp);

		//pass the address in my function &g1
		struct gps *g2;
		g2 = &g1;
		c1.parameter = (buff[gettomeditrik+14]<<8) + buff[97];
		c1.command = (buff[gettomeditrik+12]<<8) + buff[95];
		struct command *c2;
		c2 = &c1;

		check_tpye(m2, s2, g2, c2, buff, gettomeditrik);	
		
		gettomeditrik = gettomeditrik+(m1.payload + 12);
		fseek(fp, (gettomeditrik), SEEK_SET);
		fseek(fp, 0, SEEK_END);
		diffLen = ftell(fp);
		diff = gettomeditrik - diffLen;
	
		//Statement to check if there is anything left to be decoded
		if (diff == 0){
			free(buff);
			fclose(fp);
			break;			
		}
		else{
			gettomeditrik = gettomeditrik+packetheader+ethernet+ipv4+udp;
		}	
	}
}

int check_tpye(struct meditrik *m1, struct status *s1, struct gps *g1, struct command *c1, unsigned char *buff, int gettomeditrik){

	//Checking the type to decide which function will be called next
	if (m1->type == 0){
		get_status(m1, s1);
	}
	else if (m1->type == 1){
		command(m1, s1, g1, c1);
	}
	else if (m1->type == 2){
		get_gps(m1, g1);
	}
	else if (m1->type == 3){
		message(buff, m1, gettomeditrik);
	}
	return(1);
}

void get_status(struct meditrik *m1, struct status *s1){

	//Print the output of the decoder
	printf("Version: %d\n", m1->version);
	printf("Sequence: %d\n", m1->sequenceID);
	printf("From: %d\n", m1->sourceID);
	printf("To: %d\n", m1->destinationID);
	printf("Battery: %.2lf%%\n", (s1->battery * 100));
	printf("Glucose: %d\n", s1->glucose);
	printf("Capsaicin: %d\n", s1->capsaicin);
	printf("Omorfine: %d\n", s1->omorfine);
}

void command(struct meditrik *m1, struct status *s1, struct gps *g1, struct command *c1){

	int num = c1->command;

	//Using a switch statement to print only the necessary portions that the decoder needs
	switch(num){
		case 0:	
			printf("Version: %d\n", m1->version);
			printf("Sequence: %d\n", m1->sequenceID);
			printf("From: %d\n", m1->sourceID);
			printf("To: %d\n", m1->destinationID);
			printf("GET STATUS\n\n");
			break;
		case 1:
			printf("Version: %d\n", m1->version);
			printf("Sequence: %d\n", m1->sequenceID);
			printf("From: %d\n", m1->sourceID);
			printf("To: %d\n", m1->destinationID);
			s1->glucose = c1->parameter;
			printf("Glucose=%d\n\n", s1->glucose);
			break;
		case 2:
			get_gps(m1, g1);
			break;
		case 3:
			printf("Version: %d\n", m1->version);
			printf("Sequence: %d\n", m1->sequenceID);
			printf("From: %d\n", m1->sourceID);
			printf("To: %d\n", m1->destinationID);
			s1->capsaicin = c1->parameter;
			printf("Capsaicin=%d\n\n", s1->capsaicin);
			break;
		case 4:
			break;
		case 5:
			printf("Version: %d\n", m1->version);
			printf("Sequence: %d\n", m1->sequenceID);
			printf("From: %d\n", m1->sourceID);
			printf("To: %d\n", m1->destinationID);
			s1->omorfine = c1->parameter;
			printf("Omorfine=%d\n\n", s1->omorfine);
			break;
		case 6:
			break;
		case 7:
			printf("Version: %d\n", m1->version);
			printf("Sequence: %d\n", m1->sequenceID);
			printf("From: %d\n", m1->sourceID);
			printf("To: %d\n", m1->destinationID);
			printf("Re-send packet\n\n");
			break;
	}
}

void get_gps(struct meditrik *m1, struct gps *g1){

	//Print the output of the decoder
	printf("Version: %d\n", m1->version);
	printf("Sequence: %d\n", m1->sequenceID);
	printf("From: %d\n", m1->sourceID);
	printf("To: %d\n", m1->destinationID);
	printf("Latitude: %lf deg. N\n", g1->latitude);
	printf("Longitude: %lf deg. W\n", g1->longitude);
	printf("Altitude: %.0f ft.\n", (g1->altitude * 6));
}

void message(unsigned char *buff, struct meditrik *m1, int gettomeditrik){
	
	unsigned int i = 0;

	//Print the output of the decoder
	printf("Version: %d\n", m1->version);
	printf("Sequence: %d\n", m1->sequenceID);
	printf("From: %d\n", m1->sourceID);
	printf("To: %d\n", m1->destinationID);
	printf("Message: ");
	for (i = 0; i < m1->payload; i++){
		printf("%c", buff[(gettomeditrik+12) + i]);
	}
	printf("\n");
}


