#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <arpa/inet.h>

//Declaring the structs to be used in program
struct status{

	unsigned omorfine;
	unsigned capsaicin;
	unsigned glucose;
	double battery;
};

struct gps{

	float altitude;
	double latitude;
	double longitude;
};

struct command{

	unsigned command;
	unsigned int parameter;
};

struct message{

	unsigned char *message1;

};

//Creating a union
union payload {

	struct status s1;
	struct gps g1;
	struct command c1;
	struct message me1;
};

struct triforce {

	uint32_t type: 3;
	uint32_t sequenceID: 9;
	uint32_t version: 4;
};

struct meditrik{

	uint32_t destinationID;
	uint32_t sourceID;
	uint16_t totalLength;
	union fusion {
		struct triforce t1;
		short link;
	}fusion;
	union payload p1;
};


void ReadFile(int argc, char *argv[]);
void encode(char *filename, struct meditrik *m1, int version, int sequenceID, int sourceID, int destinationID, int type, unsigned char *tempbuff, unsigned char **tempbufflocation, int gettomeditrik, FILE *fp);
void encode_gps(struct meditrik *m1, int sourceID, int destinationID, FILE *fp2);
void encode_status(struct meditrik *m1, int sourceID, int destinationID, FILE *fp2);
void encode_command(struct meditrik *m1, int sourceID, int destinationID, FILE *fp2, unsigned char *tempbuff, FILE *fp);

int main(int argc, char *argv[]){

	ReadFile(argc, argv); 
	
}

void ReadFile(int argc, char *argv[]){

	//Initializing my struct
	struct meditrik m1;
	m1.destinationID = 32;
	m1.sourceID = 32;
	m1.totalLength = 16;

	//Declaring the variables to be used in the program
	char pointtohome[100];
	char filename[100];
	char tmp;
	int version = 0, sequenceID = 0, sourceID = 0, destinationID = 0, type = 0;
	int z = 0, i = 0, h = 0, count = 0, index = 0, gettomeditrik = 0;
	unsigned char *buff, *temporary, *tempbuff;
	unsigned int fileLen;
	unsigned int len, amountleft;

	FILE *fp;

	//Error checking to make sure the correct amount of arguments is entered on the command line
	if (argc != 3){
		printf("No file specified to decode.\n");
		exit(0);
	}

	//Add argv[1] to the end of the path in pointtohome
	strcpy(pointtohome, getenv("HOME"));
	strcat(pointtohome, "/ctest/decoder/");
	strcat(pointtohome, argv[1]);

	//Add argv[1] to the end of the path in filename
	strcpy(filename, argv[2]);

	//Open file
	fp = fopen(pointtohome, "r");

	//Check to see if the file opened and if not then close the program
	if (!fp){
		printf("Unable to open file.\n");
		exit(0);
	}

	//Get file length
	fseek(fp, 0, SEEK_END);
	fileLen = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	//Allocate memory
	buff = malloc(fileLen+1);
	if (!buff){
		printf("Memory error!");
    	fclose(fp);
		exit(0);
	}

	//Making sure everyting is set to 0
	memset(buff, 0, fileLen+1);

	//Allocate memory
	temporary = malloc(sizeof(unsigned char) * 1444);
	m1.p1.me1.message1 = temporary;
	if (!m1.p1.me1.message1){
		printf("Memory error!");
		free(buff);
    	fclose(fp);
		exit(0);
	}

	//Allocate memory
	tempbuff = malloc(fileLen+1);
	if (!tempbuff){
		printf("Memory error!");
		free(temporary);
		free(buff);
    	fclose(fp);
		exit(0);
	}

	//Increment count while looping and setting buff[z] to zero at the same time
	for (z = 0; z < (int)fileLen; z++){
		buff[z] = 0;
		count++;
	}

	//While EOF is not hit then buff will grab everything from the file and store it
	while((tmp = fgetc(fp)) != EOF){
		buff[index] = tmp;
		index++;
	}
	
	gettomeditrik = 82;

	//Using fseek to grab the total length of the file
	fseek(fp, 0, SEEK_END);
	fileLen = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	//Loop as long as i is less than the total length of the file searching for two consecutive '\n' characters and if
	//the loop finds this case set len to i+1 otherwise set len to the total length of the file
	while(i < (int)fileLen){
		if (buff[i] == '\n' && buff[i+1] == '\n'){
			len = i+1;
			break;
		}
		else{
			len = fileLen;
		}
		i++;
	}

	//Setting the tempbuff to the origional buff
	tempbuff = buff;

	//Setting the amountleft to figure out if the following while loop will be executed more than once
	amountleft = fileLen - len;
	fseek(fp, 0, SEEK_SET);
	
	//Loop through using strstr to check for specific strings to set type and fscan before calling the next function
	while(1){
		if(fgetc(fp) != 'V'){
			fscanf(fp, "%*[^V]V");
		}
		fseek(fp, -1, SEEK_CUR);
		if (strstr((char*)tempbuff, "GET STATUS") || strstr((char*)tempbuff, "Glucose=") || strstr((char*)tempbuff, "Omorphine=") || strstr((char*)tempbuff, "Re-send packet")){
			type = 1;
			fscanf(fp, "Version: %d\nSequence: %d\nFrom: %d\nTo: %d\n", &version, &sequenceID, &sourceID, &destinationID);
			encode(filename, &m1, version, sequenceID, sourceID, destinationID, type, tempbuff, &tempbuff, gettomeditrik, fp);
		}
		else if (strstr((char*)tempbuff, "Capsaicin=")){
			type = 1;
			fscanf(fp, "Version: %d\nSequence: %d\nFrom: %d\nTo: %d\n", &version, &sequenceID, &sourceID, &destinationID);
			encode(filename, &m1, version, sequenceID, sourceID, destinationID, type, tempbuff, &tempbuff, gettomeditrik, fp);
			tempbuff = tempbuff + 52;
		}
		else if (strstr((char*)tempbuff, "Longitude")){
			type = 2;
			fscanf(fp, "Version: %d\nSequence: %d\nFrom: %d\nTo: %d\nLongitude: %lf deg. N\nLatitude: %lf deg. W\nAltitude: %f ft.\n", &version, &sequenceID, &sourceID, &destinationID, &m1.p1.g1.longitude, &m1.p1.g1.latitude, &m1.p1.g1.altitude);
			encode(filename, &m1, version, sequenceID, sourceID, destinationID, type, tempbuff, &tempbuff, gettomeditrik, fp);
			tempbuff = tempbuff + 92;
		}		
		else if (strstr((char*)tempbuff, "Message")){
			type = 3;
			fscanf(fp, "Version: %d\nSequence: %d\nFrom: %d\nTo: %d\nMessage: %[^\n]", &version, &sequenceID, &sourceID, &destinationID, m1.p1.me1.message1);
			encode(filename, &m1, version, sequenceID, sourceID, destinationID, type, tempbuff, &tempbuff, gettomeditrik, fp);
			tempbuff = tempbuff + 43;
		}	
		else if (strstr((char*)tempbuff, "Battery")){
			fseek(fp, 1, SEEK_CUR);
			type = 0;
			fscanf(fp, "Version: %d\nSequence: %d\nFrom: %d\nTo: %d\nBattery: %lf%%\nGlucose: %u\nCapsaicin: %u\nOmorfine: %u\n", &version, &sequenceID, &sourceID, &destinationID, &m1.p1.s1.battery, &m1.p1.s1.glucose, &m1.p1.s1.capsaicin, &m1.p1.s1.omorfine);
			encode(filename, &m1, version, sequenceID, sourceID, destinationID, type, tempbuff, &tempbuff, gettomeditrik, fp);
		}
	
		//Checking the amountleft and if it is zero then free and close everything, finally breaking the loop and ending the program
		if (amountleft == 0){
			free(buff);
			free(temporary);
			fclose(fp);
			break;			
		}	
		//Sets h equal to i+1 and then uses h to look for the next pair of consecutive '\n' characters, if none are found then len is 
		//set to amountleft
		else{
			h = i+2;
			while(h < (int)fileLen){
				if (buff[h] == '\n' && buff[h+1] == '\n'){
					fseek(fp, h-1, SEEK_SET);
					len = h+1;
					break;
				} 
				else if (buff[h] == 'V' && buff[h+1] == 'e'){
					fseek(fp, h-1, SEEK_SET);
				}
				else{
					fscanf(fp, "%*[^V]Version: 1");
					fseek(fp, -11, SEEK_CUR);
					len = amountleft;
				}
				h++;
			}	
			//Sets how many zeros will be padded into the pcap file	
			gettomeditrik = 58;
			//Ensures that the proper amount is being subtracted so that the loop eventually gets broken
			amountleft = amountleft - len;
		}
	}
}

void encode(char *filename, struct meditrik *m1, int version, int sequenceID, int sourceID, int destinationID, int type, unsigned char *tempbuff, unsigned char **tempbufflocation, int gettomeditrik, FILE *fp){

	unsigned int zero = 0;
	int i = 0, tempora = 0;
	
	FILE *fp2;

	//Open file from the file path stored in filename to append
	fp2 = fopen(filename, "ab");	

	//If this file does not exist then exit the program
	if (!fp2){
		printf("Could not open.\n");
		exit(0);
	}
	else{
		;
	}

	//Write zeros into the file for how large gettomeditrik is
	for (i = 0; i < gettomeditrik; i++){
		fwrite(&zero, 1, 1, fp2);
	}	

	//Set version, sequenceID, type, fuse them, and then finally write the fusion to the file
	m1->fusion.t1.version = version;

	m1->fusion.t1.sequenceID = sequenceID;
	
	m1->fusion.t1.type = type;

	tempora = m1->fusion.t1.type;
	m1->fusion.link = htons(m1->fusion.link);

	fwrite(&m1->fusion.link, 2, 1, fp2);
	
	//Check to see what the type origionally was so that the proper subfunction can be entered, finally the tempbufflocation would be
	//set to the length of what is expected for each type
	if (tempora == 0){
		encode_status(m1, sourceID, destinationID, fp2);
		*tempbufflocation = &tempbuff[26];
		fclose(fp2);
	}
	else if (tempora == 1){
		encode_command(m1, sourceID, destinationID, fp2, tempbuff, fp);
			//Checks for the specific strings so that it can then set *tempbufflocation to the proper length
			if (strstr((char*)tempbuff, "GET STATUS")){
				*tempbufflocation = &tempbuff[55];
			}
			else if (strstr((char*)tempbuff, "Glucose=")){
				*tempbufflocation = &tempbuff[55];
			}
			else if (strstr((char*)tempbuff, "Capsaicin=")){
				*tempbufflocation = &tempbuff[5];
			}
			else if (strstr((char*)tempbuff, "Omorfine=")){
				*tempbufflocation = &tempbuff[56];
			}
			else if (strstr((char*)tempbuff, "Re-send packet")){
				*tempbufflocation = &tempbuff[59];
			}
		fclose(fp2);
	}
	else if (tempora == 2){
		encode_gps(m1, sourceID, destinationID, fp2);
		*tempbufflocation = &tempbuff[32];
		fclose(fp2);
	}
	else if (tempora == 3){
		m1->totalLength = htons(((int)strlen((char *)m1->p1.me1.message1) + 12));
		fwrite(&m1->totalLength, 2, 1, fp2);
		
		m1->sourceID = htonl(sourceID);
		fwrite(&m1->sourceID, 4, 1, fp2);

		m1->destinationID = htonl(destinationID);
		fwrite(&m1->destinationID, 4, 1, fp2);
	
		for(int z = 0; z < (int)strlen((char *)m1->p1.me1.message1); z++){
			fwrite(&m1->p1.me1.message1[z], 1, 1, fp2);
		}
		*tempbufflocation = &tempbuff[((int)strlen((char *)m1->p1.me1.message1) + 12)];
		fclose(fp2);
	}
}

void encode_status(struct meditrik *m1, int sourceID, int destinationID, FILE *fp2){

	//Converts the remaing information and then properly writes it to the file
	m1->totalLength = 26;
	m1->totalLength = htons(m1->totalLength);
	fwrite(&m1->totalLength, 2, 1, fp2);

	m1->sourceID = htonl(sourceID);
	fwrite(&m1->sourceID, 4, 1, fp2);

	m1->destinationID = htonl(destinationID);
	fwrite(&m1->destinationID, 4, 1, fp2);

	m1->p1.s1.battery = m1->p1.s1.battery/100;
	fwrite(&m1->p1.s1.battery, 8, 1, fp2);

	m1->p1.s1.glucose = htons(m1->p1.s1.glucose);
	fwrite(&m1->p1.s1.glucose, 2, 1, fp2);

	m1->p1.s1.capsaicin = htons(m1->p1.s1.capsaicin);
	fwrite(&m1->p1.s1.capsaicin, 2, 1, fp2);

	m1->p1.s1.omorfine = htons(m1->p1.s1.omorfine);
	fwrite(&m1->p1.s1.omorfine, 2, 1, fp2);
}

void encode_command(struct meditrik *m1, int sourceID, int destinationID, FILE *fp2, unsigned char *tempbuff, FILE *fp){

	int ver = 0, seqID = 0, sourID = 0, dID = 0, parameter = 0;

	//Seeks back to the begging 
	fseek(fp, 0, SEEK_SET);

	//Checks for the string captured by the strstr in order to set the command and/or parameter and/or totalLength
	if (strstr((char*)tempbuff, "GET STATUS")){
		m1->p1.c1.command = 0;
		m1->totalLength = 14;
	}
	else if (strstr((char*)tempbuff, "Glucose=")){
		fscanf(fp, "Version: %d\nSequence: %d\nFrom: %d\nTo: %d\nGlucose=%d\n", &ver, &seqID, &sourID, &dID, &parameter);
		m1->p1.c1.command = 1;
		m1->p1.c1.parameter = parameter;
		m1->totalLength = 16;
	}
	else if (strstr((char*)tempbuff, "Capsaicin=")){
		fscanf(fp, "Version: %d\nSequence: %d\nFrom: %d\nTo: %d\nCapsaicin=%d\n", &ver, &seqID, &sourID, &dID, &parameter);
		m1->p1.c1.command = 3;
		m1->p1.c1.parameter = parameter;
		m1->totalLength = 16;
	}
	else if (strstr((char*)tempbuff, "Omorfine=")){
		fscanf(fp, "Version: %d\nSequence: %d\nFrom: %d\nTo: %d\nOmorfine=%d\n", &ver, &seqID, &sourID, &dID, &parameter);
		m1->p1.c1.command = 5;
		m1->p1.c1.parameter = parameter;
		m1->totalLength = 16;
	}
	else if (strstr((char*)tempbuff, "Re-send packet")){
		m1->p1.c1.command = 7;
		m1->p1.c1.parameter = 0;
		m1->totalLength = 14;
	}

	//Converts the remaing information and then properly writes it to the file
	m1->totalLength = htons(m1->totalLength);
	fwrite(&m1->totalLength, 2, 1, fp2);

	m1->sourceID = htonl(sourceID);
	fwrite(&m1->sourceID, 4, 1, fp2);

	m1->destinationID = htonl(destinationID);
	fwrite(&m1->destinationID, 4, 1, fp2);

	m1->p1.c1.command = htons(m1->p1.c1.command);
	fwrite(&m1->p1.c1.command, 2, 1, fp2);

	if (htons(m1->totalLength) == 16){
		m1->p1.c1.parameter = htons(m1->p1.c1.parameter);
		fwrite(&m1->p1.c1.parameter, 2, 1, fp2);
	}
}

void encode_gps(struct meditrik *m1, int sourceID, int destinationID, FILE *fp2){

	//Converts the remaing information and then properly writes it to the file
	m1->totalLength = 32;
	m1->totalLength = htons(m1->totalLength);
	fwrite(&m1->totalLength, 2, 1, fp2);

	m1->sourceID = htonl(sourceID);
	fwrite(&m1->sourceID, 4, 1, fp2);

	m1->destinationID = htonl(destinationID);
	fwrite(&m1->destinationID, 4, 1, fp2);

	fwrite(&m1->p1.g1.longitude, sizeof(m1->p1.g1.longitude), 1, fp2);

	fwrite(&m1->p1.g1.latitude, sizeof(m1->p1.g1.latitude), 1, fp2);
	
	m1->p1.g1.altitude = m1->p1.g1.altitude/6;
	fwrite(&m1->p1.g1.altitude, sizeof(m1->p1.g1.altitude), 1, fp2);
}
